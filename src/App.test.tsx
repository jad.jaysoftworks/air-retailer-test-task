import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import App from './App';

test('Page Renders', () => {
  render(<App />);
  const linkElement = screen.getByText(/Frontend Task By Jad/i);
  expect(linkElement).toBeInTheDocument();
});

test('Chart Flow Works', () => {
  render(<App />);
  const button = screen.getByTestId("generateChartBtn");
  fireEvent.click(button);
  expect(screen.getByText('Loading...')).toBeInTheDocument();
})

test('Chart Loads', () => {
  render(<App />);
  const button = screen.getByTestId("generateChartBtn");
  fireEvent.click(button);
  setTimeout(() => {
    expect(screen.getByTestId('chart-canvas')).toBeInTheDocument();
  });
})
