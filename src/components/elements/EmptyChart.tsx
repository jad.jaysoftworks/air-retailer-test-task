import React from "react";
import '../../scss/chart.scss';

export function EmptyChart(isLoading = false) {
  return (
    <div className="card-elements">
      <h3>
        {isLoading ? "Loading..." : "Click To Fetch USDT_BTC Data"}
      </h3>
    </div>
  )
}
