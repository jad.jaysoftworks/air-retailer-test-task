import React, {useState} from "react";
import {EmptyChart} from "./elements/EmptyChart";
import {BinanceApiElement, PoloniexApiElement} from "../interfaces/api";

class CandleChart {
  ctx: CanvasRenderingContext2D;
  width: number;
  height: number;
  heightPercentage: number;
  options = {
    margin: 10,
    lineWidth: 5,
    candleColorB: "red",
    candleColorS: "green"
  }
  data: {o: number, h: number, l: number, c: number}[];

  constructor(canvas: HTMLCanvasElement, data: PoloniexApiElement[], options: any = {}) {
    this.ctx = canvas.getContext("2d") as CanvasRenderingContext2D;
    this.width = canvas.width;
    this.height = canvas.height;

    this.heightPercentage = this.height / 100;

    this.options.margin = options.margin || 10;
    this.options.lineWidth = options.lineWidth || 5;
    // Color downward candles
    this.options.candleColorB = options.candleColorB || "red";
    // Color rising candles
    this.options.candleColorS = options.candleColorS || "green";
    // Data to be plotted
    this.data = this.formatData(data);
  }

  // Format Data
  formatData(data: PoloniexApiElement[]) {
    let max = 0;
    let min = 0;
    let stat = 0;
    let ps = 0;

    data.forEach(el => {
      const h = el.high;
      const l = el.low;
      // Находим максимальную цену
      max = h > max ? h : max;
      // Находим минимальную цену
      min = !min || l < min ? l : min;
    });

    stat = max - min;
    ps = stat / 100;

    return data.map(el => {
      return {
        l: (100 - (stat - (max - el.low)) / ps) * this.heightPercentage,
        h: (100 - (stat - (max - el.high)) / ps) * this.heightPercentage,
        o: (100 - (stat - (max - el.open)) / ps) * this.heightPercentage,
        c: (100 - (stat - (max - el.close)) / ps) * this.heightPercentage
      };
    });
  }


  // Draw candles on canvas
  draw() {
    this.ctx.clearRect(0, 0, this.width, this.height);
    const wP = this.width / this.data.length;

    this.data.forEach((el, i) => {
      this.ctx.strokeStyle =
        el.c > el.o
          ? this.options.candleColorB
          : el.c < el.o
            ? this.options.candleColorS
            : "white";

      let x = i * (this.width / this.data.length);
      x += this.width / this.data.length / 2;

      let yO = this.data[i].o;
      let yC = this.data[i].c;

      this.ctx.lineWidth = (wP / 100) * this.options.lineWidth;

      yC = yO === yC ? yC + this.ctx.lineWidth : yC;

      this.ctx.beginPath();
      this.ctx.moveTo(x, this.data[i].h);
      this.ctx.lineTo(x, this.data[i].l);
      this.ctx.stroke();

      this.ctx.lineWidth = (wP / 100) * (100 - this.options.margin);
      this.ctx.beginPath();
      this.ctx.moveTo(x, Number(yO));
      this.ctx.lineTo(x, Number(yC));
      this.ctx.stroke();
    });

    return this;
  }
}

export function Chart() {
  const [chartOpened, setChartOpened] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [canvasWidth, setCanvasWidth] = useState(window.innerWidth - 40);
  const [canvasHeight, setCanvasHeight] = useState(window.innerHeight - 80);

  const toggleChart = async () => {
    if (!chartOpened && !isLoading) {
      setIsLoading(true);
      setChartOpened(true);
      // const data = await fetchData() as BinanceApiElement[];
      const data = await fetchData() as PoloniexApiElement[];
      setIsLoading(false);
      setTimeout(() => {
        console.log('Received Data: ', data);
        generateChart(data);
      }, 300);
    }
  }

  // On Resize ( Resize the canvas )
  window.addEventListener('resize', () => {
    setCanvasWidth(window.innerWidth - 40);
    setCanvasHeight(window.innerHeight - 80);
  });

  // Fetch data from API
  const fetchData = async () => {
    const response = await fetch("https://poloniex.com/public?command=returnChartData&currencyPair=USDT_BTC&start=1546300800&end=1546646400&period=14400");
    return await response.json();
  }

  // Chart Functions
  const generateChart = (data: PoloniexApiElement[]) => {
    const canvas = document.getElementById('chart-canvas') as HTMLCanvasElement;
    const newArray = data.slice(0, 30);
    const chart = new CandleChart(canvas, newArray);
    chart.draw();
  }

  return (
    <div className="chart-div">
      <div id="chart" className="chart-container">
        <div className={chartOpened && !isLoading? "" : "empty-card centered"}>
          { !chartOpened || isLoading? EmptyChart(isLoading) :
            <div>
              <canvas className="canvas" id="chart-canvas" height={canvasHeight} width={canvasWidth}></canvas>
            </div>
          }
          {
            !chartOpened && !isLoading? <button data-testid="generateChartBtn" onClick={toggleChart}>Generate Chart</button> : ''
          }
        </div>
      </div>
    </div>
  )
}
