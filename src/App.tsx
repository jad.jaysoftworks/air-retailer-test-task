import React from 'react';

// Assets
import './App.scss';
import jayLaptop from './assets/images/jayLaptop.webp';

// Components
import Header from "./components/Header";
import {Chart} from "./components/Chart";

function App() {
  return (
    <div className="App">
      <a href="https://jadharmoush.com" className="author-link">
        <img src={jayLaptop} alt="Jad's Logo"></img>
      </a>
      <Header/>
      <Chart/>
    </div>
  );
}

export default App;
